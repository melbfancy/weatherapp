require 'test_helper'

class ChoiceTest < ActiveSupport::TestCase
	fixtures :choices
	fixtures :advices
	test "choice cannot be empty" do 
		choice = Choice.create
		assert choice.invalid?
	end
	test "choice cannot be created if lack of content" do
		choice = Choice.create(message: "test message")
		assert choice.invalid?
	end
	test "choice cannot be created if lack of message" do
		choice = Choice.create(content: "test cotent")
		assert choice.invalid?
	end
	test "choice can be created only if BOTH content and message are provided" do 
		choice = Choice.create(content: 'test content', message: "test message")
		assert choice.valid?
	end
	test "choice can have advice id" do 
		choice = Choice.create(advice_id: advices(:one), content: 'test content', message: "test message")
		assert choice.valid?
	end
end
