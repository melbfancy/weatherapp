require 'test_helper'

class AdviceTest < ActiveSupport::TestCase
  test "should not save without a question" do
  	advice = Advice.new
    assert_not advice.save
  end
end
