require 'test_helper'

class Event_creatorTest < ActiveSupport::TestCase
  setup do
  	@weather_data = Array.new
  	@weather_data << {stationID: "1", 
                      minTemps: [16,16,11,12,15,16,17], 
                      maxTemps: [19,20,21,26,27,25,22]}

  	@weather_data << {stationID: "1", 
                      minTemps: [16,16,16,14,13,16,21], 
                      maxTemps: [19,20,24,26,27,22,19]}
    

    Station.create!(
          station_bom_id: "1",
          name: "Testing Station",
          region: nil,
          ave_max_jan: 20,
          ave_min_jan: 20,
          ave_max_feb: 20,
          ave_min_feb: 20,
          ave_max_mar: 20,
          ave_min_mar: 20,
          ave_max_apr: 20,
          ave_min_apr: 20,
          ave_max_may: 20,
          ave_min_may: 20,
          ave_max_june: 20,
          ave_min_june: 20,
          ave_max_july: 20,
          ave_min_july: 20,
          ave_max_aug: 20,
          ave_min_aug: 20,
          ave_max_sept: 20,
          ave_min_sept: 20,
          ave_max_oct: 20,
          ave_min_oct: 20,
          ave_max_nov: 20,
          ave_min_nov: 20,
          ave_max_dec: 20,
          ave_min_dec: 20,
          created_at: "2015-09-22 17:00:01",
    	  updated_at: "2015-09-22 17:00:01"
          )
    @ruleOne = {id: 1,
          higher_than_avg: 5,
          lower_than_avg: 5,
          hot_or_cold: true,
          length: 3,
          tem_dif: 5,
          rule_type: "Multi Day",
          station_id: Station.last[:id]}

    @ruleTwo = {id: 2,
          higher_than_avg: 5,
          lower_than_avg: 5,
          hot_or_cold: true,
          length: 3,
          tem_dif: 5,
          rule_type: "One Day",
          station_id: Station.last[:id]}

    @ruleThree = {id: 3,
          higher_than_avg: 5,
          lower_than_avg: 5,
          hot_or_cold: false,
          length: 3,
          tem_dif: 5,
          rule_type: "Multi Day",
          station_id: Station.last[:id]}

    @ruleFour = {id: 4,
          higher_than_avg: 5,
          lower_than_avg: 5,
          hot_or_cold: false,
          length: 3,
          tem_dif: 5,
          rule_type: "One Day",
          station_id: Station.last[:id]}

    
    Event_creator.check_weather_event_removal

  end

  test "checking setup" do
  	if Station.last[:station_bom_id] != "1"
  		assert false
  	end
  end

  test "the tester for event-heatwave succeed" do
  	assert_difference('WeatherEvent.count') do
  		Event_creator.check_specific_rule(@ruleOne,@weather_data[0])
  	end
  end

  test "the tester for event-heaspike succeed" do
  	assert_difference('WeatherEvent.count') do
  		Event_creator.check_specific_rule(@ruleTwo,@weather_data[0])
  	end
  end

  test "the tester for event-coldsnap succeed" do
    assert_difference('WeatherEvent.count') do
      Event_creator.check_specific_rule(@ruleThree,@weather_data[0])
    end
  end

  test "the tester for event-coldspike succeed" do
    assert_difference('WeatherEvent.count') do
      Event_creator.check_specific_rule(@ruleFour,@weather_data[0])
    end
  end

  test "the tester for event-heatwave fail" do
    assert_difference('WeatherEvent.count',0) do
      Event_creator.check_specific_rule(@ruleOne,@weather_data[1])
    end
  end

  test "the tester for event-heatspike fail" do
    assert_difference('WeatherEvent.count',0) do
      Event_creator.check_specific_rule(@ruleTwo,@weather_data[1])
    end
  end

  test "the tester for event-coldsnap fail" do
    assert_difference('WeatherEvent.count',0) do
      Event_creator.check_specific_rule(@ruleThree,@weather_data[1])
    end
  end

  test "the tester for event-coldspike fail" do
    assert_difference('WeatherEvent.count',0) do
      Event_creator.check_specific_rule(@ruleFour,@weather_data[1])
    end
  end

  test "the tester for event-removal of past event" do
    WeatherEvent.create(time_start: "2015-09-22 18:28:21",
            time_end: "2015-09-22 18:28:21",
            rule_id: 1,
            time_created: "2015-09-22 18:28:21",
            time_modified: "2015-09-22 18:28:21",
            created_at: "2015-09-22 18:28:21",
            updated_at: "2015-09-22 18:28:21")
    assert_difference('WeatherEvent.count',-1) do
      Event_creator.check_weather_event_removal
    end
  end

  test "the tester for event-removal of a duplicate event" do
    WeatherEvent.create(time_start: "2120-09-23 18:28:21",
            time_end: "2120-09-24 18:28:21",
            rule_id: 1,
            time_created: "2015-09-22 18:28:21",
            time_modified: "2015-09-22 18:28:21",
            created_at: "2015-09-22 18:28:21",
            updated_at: "2015-09-22 18:28:21")
    WeatherEvent.create(time_start: "2120-09-23 18:28:21",
            time_end: "2120-09-24 18:28:21",
            rule_id: 1,
            time_created: "2015-09-22 18:28:21",
            time_modified: "2015-09-22 18:28:21",
            created_at: "2015-09-22 18:28:21",
            updated_at: "2015-09-22 18:28:21")
    assert_difference('WeatherEvent.count',-1) do
      Event_creator.check_weather_event_removal
    end
  end

end