require 'test_helper'

class FeedbackTest < ActiveSupport::TestCase

  test "Feedback attributes must not be empty" do
    feedback = Feedback.new
    assert feedback.invalid?
    assert feedback.errors[:title].any?
    assert feedback.errors[:description].any?
  end

end
