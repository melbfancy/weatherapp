require 'test_helper'

class UserAdviceTest < ActiveSupport::TestCase
	fixtures :advices
	fixtures :users
	test "user advice can be created" do 
		user_advice = UserAdvice.create
		assert user_advice.valid?
	end
	test "user advice can have value" do 
		user_advice = UserAdvice.create(value: 'test value')
		assert user_advice.valid?
	end
	test "user advice can have advice id" do 
		user_advice = UserAdvice.create(advice_id: advices(:one))
		assert user_advice.valid?
	end
	test "user advice can have user id" do 
		user_advice = UserAdvice.create(user_id: users(:one))
		assert user_advice.valid?
	end
end
