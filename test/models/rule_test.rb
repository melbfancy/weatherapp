require 'test_helper'

class RuleTest < ActiveSupport::TestCase
  test "should not save without a type" do
  	rule = Rule.new
    assert_not rule.save
  end

  test "rule is not valid without a numerical tem_dif" do 
  	rule1 = Rule.new(description: "test rule 1",
										  high_tem: 0,
										  low_tem: 0,
										  higher_than_avg: 0,
										  lower_than_avg: 0,
										  hot_or_cold: "t",
										  length: 1,
										  tem_dif: "aaa",
										  rule_type: "One Day",
										  station_id: 80)
		assert rule1.invalid?
	end

	test "rule is not valid without a numerical length" do 
  	rule2 = Rule.new(description: "test rule 2",
										  high_tem: 0,
										  low_tem: 0,
										  higher_than_avg: 0,
										  lower_than_avg: 0,
										  hot_or_cold: "t",
										  length: "aaa",
										  tem_dif: 1,
										  rule_type: "One Day",
										  station_id: 80)
		assert rule2.invalid?
	end

	test "rule is not valid without a numerical higer/lower than avg" do 
  	rule3 = Rule.new(description: "test rule 3",
										  high_tem: 0,
										  low_tem: 0,
										  higher_than_avg: "aaa",
										  lower_than_avg: "aaa",
										  hot_or_cold: "t",
										  length: 1,
										  tem_dif: 1,
										  rule_type: "One Day",
										  station_id: 80)
		assert rule3.invalid?
	end

	test "rule is not valid without a higer/lower than avg higher than or equal to 0.0" do 
  	rule4 = Rule.new(description: "test rule 3",
										  high_tem: 0,
										  low_tem: 0,
										  higher_than_avg: -1,
										  lower_than_avg: -1,
										  hot_or_cold: "t",
										  length: 1,
										  tem_dif: 1,
										  rule_type: "One Day",
										  station_id: 80)
		assert rule4.invalid?
	end
end
