require 'test_helper'

class MessageTest < ActiveSupport::TestCase
	fixtures :users
	test "message can be created" do 
		message = Message.create
		assert message.valid?
	end
	test "message can have content" do 
		message = Message.create(content: 'test content')
		assert message.valid?
	end
	test "message can have sent marker" do 
		message = Message.create(sent: true)
		assert message.valid?
	end
	test "message can have sent time" do 
		message = Message.create(time_sent: DateTime.now)
		assert message.valid?
	end
	test "message can have receiver" do 
		message = Message.create(user_id: users(:one))
		assert message.valid?
	end
end
