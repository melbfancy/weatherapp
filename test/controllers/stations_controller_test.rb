require 'test_helper'

class StationsControllerTest < ActionController::TestCase
  setup do
    sign_in admins(:user1)
    @stations = stations(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:stations)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create stations" do
    assert_difference('Station.count') do
      post :create, station: { name: @stations.name,
      							  station_bom_id: @stations.station_bom_id,
								  region: @stations.region,
								  ave_max_jan: @stations.ave_max_jan,
								  ave_min_jan: @stations.ave_min_jan,
								  ave_max_feb: @stations.ave_max_feb,
								  ave_min_feb: @stations.ave_min_feb,
								  ave_max_mar: @stations.ave_max_mar,
								  ave_min_mar: @stations.ave_min_mar,
								  ave_max_apr: @stations.ave_max_apr,
								  ave_min_apr: @stations.ave_min_apr,
								  ave_max_may: @stations.ave_max_may,
								  ave_min_may: @stations.ave_min_may,
								  ave_max_june: @stations.ave_max_june,
								  ave_min_june: @stations.ave_min_june,
								  ave_max_july: @stations.ave_max_july,
								  ave_min_july: @stations.ave_min_july,
								  ave_max_aug: @stations.ave_max_aug,
								  ave_min_aug: @stations.ave_min_aug,
								  ave_max_sept: @stations.ave_max_sept,
								  ave_min_sept: @stations.ave_min_sept,
								  ave_max_oct: @stations.ave_max_oct,
								  ave_min_oct: @stations.ave_min_oct,
								  ave_max_nov: @stations.ave_max_nov,
								  ave_min_nov: @stations.ave_min_nov,
								  ave_max_dec: @stations.ave_max_dec,
								  ave_min_dec: @stations.ave_min_dec }
    end

    assert_redirected_to station_path(assigns(:station))
  end

  test "should show stations" do
    get :show, id: @stations
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @stations
    assert_response :success
  end

  test "should update stations" do
    patch :update, id: @stations, station: { name: @stations.name,
    										  station_bom_id: @stations.station_bom_id,
											  region: @stations.region,
											  ave_max_jan: @stations.ave_max_jan,
											  ave_min_jan: @stations.ave_min_jan,
											  ave_max_feb: @stations.ave_max_feb,
											  ave_min_feb: @stations.ave_min_feb,
											  ave_max_mar: @stations.ave_max_mar,
											  ave_min_mar: @stations.ave_min_mar,
											  ave_max_apr: @stations.ave_max_apr,
											  ave_min_apr: @stations.ave_min_apr,
											  ave_max_may: @stations.ave_max_may,
											  ave_min_may: @stations.ave_min_may,
											  ave_max_june: @stations.ave_max_june,
											  ave_min_june: @stations.ave_min_june,
											  ave_max_july: @stations.ave_max_july,
											  ave_min_july: @stations.ave_min_july,
											  ave_max_aug: @stations.ave_max_aug,
											  ave_min_aug: @stations.ave_min_aug,
											  ave_max_sept: @stations.ave_max_sept,
											  ave_min_sept: @stations.ave_min_sept,
											  ave_max_oct: @stations.ave_max_oct,
											  ave_min_oct: @stations.ave_min_oct,
											  ave_max_nov: @stations.ave_max_nov,
											  ave_min_nov: @stations.ave_min_nov,
											  ave_max_dec: @stations.ave_max_dec,
											  ave_min_dec: @stations.ave_min_dec}
    assert_redirected_to station_path(assigns(:station))
  end

  test "should destroy station" do
    assert_difference('Station.count', -1) do
      delete :destroy, id: @stations
    end

    assert_redirected_to stations_path
  end   
end
