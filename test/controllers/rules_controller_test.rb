require 'test_helper'

class RulesControllerTest < ActionController::TestCase
  setup do
    sign_in admins(:user1)
    @rules = rules(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:rules)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create rules" do
    assert_difference('Rule.count') do
      post :create, rule: { description: @rules.description,
										  high_tem: @rules.high_tem,
										  low_tem: @rules.low_tem,
										  higher_than_avg: @rules.higher_than_avg,
										  lower_than_avg: @rules.lower_than_avg,
										  hot_or_cold: @rules.hot_or_cold,
										  length: @rules.length,
										  tem_dif: @rules.tem_dif,
										  rule_type: @rules.rule_type,
										  station_id: @rules.station_id }
    end

    assert_redirected_to rule_path(assigns(:rule))
  end

  test "should show rules" do
    get :show, id: @rules
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @rules
    assert_response :success
  end

  test "should update rules" do
    patch :update, id: @rules, rule: { description: @rules.description,
										  high_tem: @rules.high_tem,
										  low_tem: @rules.low_tem,
										  higher_than_avg: @rules.higher_than_avg,
										  lower_than_avg: @rules.lower_than_avg,
										  hot_or_cold: @rules.hot_or_cold,
										  length: @rules.length,
										  tem_dif: @rules.tem_dif,
										  rule_type: @rules.rule_type,
										  station_id: @rules.station_id}
    assert_redirected_to rule_path(assigns(:rule))
  end

  test "should destroy rule" do
    assert_difference('Rule.count', -1) do
      delete :destroy, id: @rules
    end

    assert_redirected_to rules_path
  end  
end
