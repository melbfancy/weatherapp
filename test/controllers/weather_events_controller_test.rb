require 'test_helper'

class WeatherEventsControllerTest < ActionController::TestCase
  setup do
    sign_in admins(:user1)
    @weather_events = weather_events(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:weather_events)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create weather_events" do
    assert_difference('WeatherEvent.count') do
      post :create, weather_event: { time_start: @weather_events.time_start, 
      									time_end: @weather_events.time_end, 
      									time_created: @weather_events.time_created, 
      									time_modified: @weather_events.time_modified, 
      									rule_id: @weather_events.rule_id,
      									created_at: @weather_events.created_at, 
      									updated_at: @weather_events.updated_at }
    end

    assert_redirected_to weather_event_path(assigns(:weather_event))
  end

  test "should show weather_events" do
    get :show, id: @weather_events
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @weather_events
    assert_response :success
  end

  test "should update weather_events" do
    patch :update, id: @weather_events, weather_event: { time_start: @weather_events.time_start, 
					      									time_end: @weather_events.time_end, 
					      									time_created: @weather_events.time_created, 
					      									time_modified: @weather_events.time_modified, 
					      									rule_id: @weather_events.rule_id,
					      									created_at: @weather_events.created_at, 
					      									updated_at: @weather_events.updated_at }
    assert_redirected_to weather_event_path(assigns(:weather_event))
  end

  test "should destroy weather_events" do
    assert_difference('WeatherEvent.count', -1) do
      delete :destroy, id: @weather_events
    end

    assert_redirected_to weather_events_path
  end
end
