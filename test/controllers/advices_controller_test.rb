require 'test_helper'

class AdvicesControllerTest < ActionController::TestCase

	setup do
    sign_in admins(:user1)
    @advices = advices(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:advices)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create advices" do
    assert_difference('Advice.count') do
      post :create, advice: { description: @advices.description, question: @advices.question }
    end

    assert_redirected_to advice_path(assigns(:advice))
  end

  test "should show advices" do
    get :show, id: @advices
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @advices
    assert_response :success
  end

  test "should update advices" do
    patch :update, id: @advices, advice: { description: @advices.description, question: @advices.question }
    assert_redirected_to advice_path(assigns(:advice))
  end

  test "should destroy advice" do
    assert_difference('Advice.count', -1) do
      delete :destroy, id: @advices
    end

    assert_redirected_to advices_path
  end
  
end
