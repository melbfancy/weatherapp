require 'test_helper'

class RuleAdvicesControllerTest < ActionController::TestCase
  setup do

    sign_in admins(:user1)
    @rule_advice = rule_advices(:one)
    @rule = rules(:one)
    @advice = advices(:one)
    @rule.rule_advices << @rule_advice
    @advice.rule_advices << @rule_advice
    @rule_advice.rule = @rule
    @rule_advice.advice = @advice

  end

  test "should get index" do
    get :index, rule_id: @rule.id
    assert_response :success
    assert_not_nil assigns(:rule_advices)
  end

  test "should get edit" do
    get :edit, rule_id: @rule.id, id: @rule.rule_advices.first
    assert_response :success
  end

  test "should update user_advice" do
    patch :update, rule_id: @rule.id, id: @rule.rule_advices.first, rule_advice: { value: "yes" }
    assert_redirected_to rule_rule_advices_path(assigns(:rule_advice).rule)
  end

  test "rule advices should not be nil" do
    assert_not_nil @rule.rule_advices.first
  end

  test "question should not be nil" do
    assert_not_nil @rule.rule_advices.first.advice.question
  end

  test "size should be 1" do
  	assert @rule.rule_advices.size == 1
  end

end
