require 'test_helper'

class UserAdvicesControllerTest < ActionController::TestCase
  setup do
    @user = users(:one)
    sign_in @user
    @user_advice = user_advices(:one)
    @advice = advices(:one)
    @user.user_advices << @user_advice
    @advice.user_advices  << @user_advice
    @user_advice.user = @user
    @user_advice.advice = @advice
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:user_advices)
  end

  test "should get edit" do
    get :edit, id: @user_advice
    assert_response :success
  end

  test "should update user_advice" do
    patch :update, id: @user_advice, user_advice: { value: @user_advice.value, user_id: @user_advice.user_id, advice_id: @user_advice.advice_id }
    assert_redirected_to user_advices_path
  end

end
