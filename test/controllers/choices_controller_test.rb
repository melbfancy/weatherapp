require 'test_helper'

class ChoicesControllerTest < ActionController::TestCase
fixtures :advices
fixtures :choices
setup do
    sign_in admins(:user1)
    @choice = choices(:one)
    @advice = advices(:one)
    @advice.choices << @choice
    @choice.advice = @advice
  end

  test "should create choice" do
    assert_difference('Choice.count') do
      post :create, id: @choice, advice_id: @advice, choice: {id: @choice, content: @choice.content, message: @choice.message }
    end

    assert_redirected_to advice_path(assigns(:advice))
  end

  test "should destroy choice" do  	
    assert_difference('Choice.count', -1) do
      delete :destroy, advice_id: @advice, id: @choice
    end

    assert_redirected_to advice_path(assigns(:advice))
  end
end