require 'test_helper'

class AdminsControllerTest < ActionController::TestCase
   setup do
    
    @admins = admins(:user2)
    sign_in admins(:user1)
  end

  # test "should get index" do
  #   get :index
  #   assert_response :success
  #   assert_not_nil assigns(:admins)
  # end

  # test "should get new" do
  #   get :new
  #   assert_response :success
  # end

  # test "should create stations" do
  #   assert_difference('Admin.count') do
  #     post :create, admin: { email: @admins.email,
		# 					  encrypted_password: @admins.encrypted_password,
		# 					  sign_in_count: @admins.sign_in_count,
		# 					  reset_password_token: @admins.reset_password_token,
		# 					  reset_password_sent_at: @admins.reset_password_sent_at,
		# 					  remember_created_at: @admins.remember_created_at,
		# 					  current_sign_in_at: @admins.current_sign_in_at,
		# 					  last_sign_in_at: @admins.last_sign_in_at,
		# 					  current_sign_in_ip: @admins.current_sign_in_ip,
		# 					  last_sign_in_ip: @admins.last_sign_in_ip}
  #   end

  #   assert_redirected_to admin_path(assigns(:admin))
  # end

  test "should show admins" do
    get :show, id: @admins
    assert_response :success
  end

  # test "should get edit" do
  #   get :edit, id: @admins
  #   assert_response :success
  # end

  # test "should update admins" do
  #   patch :update, id: @admins, admin: { email: @admins.email,
		# 					  encrypted_password: @admins.encrypted_password,
		# 					  sign_in_count: @admins.sign_in_count,
		# 					  reset_password_token: @admins.reset_password_token,
		# 					  reset_password_sent_at: @admins.reset_password_sent_at,
		# 					  remember_created_at: @admins.remember_created_at,
		# 					  current_sign_in_at: @admins.current_sign_in_at,
		# 					  last_sign_in_at: @admins.last_sign_in_at,
		# 					  current_sign_in_ip: @admins.current_sign_in_ip,
		# 					  last_sign_in_ip: @admins.last_sign_in_ip}
  #   assert_redirected_to admin_path(assigns(:admin))
  # end

  # test "should destroy admin" do
  #   assert_difference('Admin.count', -1) do
  #     delete :destroy, id: @admins
  #   end

  #   assert_redirected_to admins_path
  # end
end
