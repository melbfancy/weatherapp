require 'test_helper'

class UsersControllerTest < ActionController::TestCase
  setup do
  	sign_in admins(:user1)
    sign_in users(:one)
    @user = users(:two)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:users)
  end

  test "should show user" do
    get :show, id: @user
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @user
    assert_response :success
  end

  test "should update users" do
    patch :update, id: @user, user: { firstname: "AnotherString" }
    assert_redirected_to users_path
  end

end
