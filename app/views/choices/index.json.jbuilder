json.array!(@choices) do |choice|
  json.extract! choice, :id, :content, :message, :advice_id
  json.url choice_url(choice, format: :json)
end
