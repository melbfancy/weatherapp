json.array!(@weather_events) do |weather_event|
  json.extract! weather_event, :id, :time_start, :time_end, :time_created, :time_modified, :rule_id
  json.url weather_event_url(weather_event, format: :json)
end
