json.array!(@user_advices) do |user_advice|
  json.extract! user_advice, :id, :value, :user_id, :advice_id
  json.url user_advice_url(user_advice, format: :json)
end
