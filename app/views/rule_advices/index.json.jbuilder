json.array!(@rule_advices) do |rule_advice|
  json.extract! rule_advice, :id, :value, :rule_id, :advice_id
  json.url rule_advice_url(rule_advice, format: :json)
end
