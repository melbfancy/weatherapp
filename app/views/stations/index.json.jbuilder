json.array!(@stations) do |station|
  json.extract! station, :id, :name, :station_bom_id, :region, :ave_max_jan, :ave_min_jan, :ave_max_feb, :ave_min_feb, :ave_max_mar, :ave_min_mar, :ave_max_apr, :ave_min_apr, :ave_max_may, :ave_min_may, :ave_max_june, :ave_min_june, :ave_max_july, :ave_min_july, :ave_max_aug, :ave_min_aug, :ave_max_sept, :ave_min_sept, :ave_max_oct, :ave_min_oct, :ave_max_nov, :ave_min_nov, :ave_max_dec, :ave_min_dec
  json.url station_url(station, format: :json)
end
