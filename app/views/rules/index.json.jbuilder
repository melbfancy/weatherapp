json.array!(@rules) do |rule|
  json.extract! rule, :id, :description, :high_tem, :low_tem, :higher_than_avg, :lower_than_avg, :station_id
  json.url rule_url(rule, format: :json)
end
