class WeatherEventsController < ApplicationController
  before_action :set_weather_event, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_admin!

  # GET /weather_events
  # GET /weather_events.json
  def index
    @weather_events = WeatherEvent.all
  end

  # GET /weather_events/1
  # GET /weather_events/1.json
  def show
  end

  # GET /weather_events/new
  def new
    @weather_event = WeatherEvent.new
  end

  # GET /weather_events/1/edit
  def edit
  end

  # POST /weather_events
  # POST /weather_events.json
  def create
    @weather_event = WeatherEvent.new(weather_event_params)

    respond_to do |format|
      if @weather_event.save
        format.html { redirect_to @weather_event, notice: 'Weather event was successfully created.' }
        format.json { render :show, status: :created, location: @weather_event }
      else
        format.html { render :new }
        format.json { render json: @weather_event.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /weather_events/1
  # PATCH/PUT /weather_events/1.json
  def update
    respond_to do |format|
      if @weather_event.update(weather_event_params)
        format.html { redirect_to @weather_event, notice: 'Weather event was successfully updated.' }
        format.json { render :show, status: :ok, location: @weather_event }
      else
        format.html { render :edit }
        format.json { render json: @weather_event.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /weather_events/1
  # DELETE /weather_events/1.json
  def destroy
    @weather_event.destroy
    respond_to do |format|
      format.html { redirect_to weather_events_url, notice: 'Weather event was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_weather_event
      @weather_event = WeatherEvent.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def weather_event_params
      params.require(:weather_event).permit(:time_start, :time_end, :time_created, :time_modified, :rule_id)
    end
end
