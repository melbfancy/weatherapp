class RuleAdvicesController < ApplicationController
  before_action :set_rule_advice, only: [:edit, :update]
  before_action :authenticate_admin!
  # GET /rule_advices
  # GET /rule_advices.json
  def index
    rule = Rule.find(params[:rule_id])
    @rule_advices = rule.rule_advices 
  end

  # GET /rule_advices/1/edit
  def edit
    @rule = Rule.find(params[:rule_id])
  end

  # PATCH/PUT /rule_advices/1
  # PATCH/PUT /rule_advices/1.json
  def update
    respond_to do |format|
      if @rule_advice.update(rule_advice_params)
        format.html { redirect_to rule_rule_advices_path(@rule_advice.rule), notice: 'Rule advice was successfully updated.' }
        format.json { render :show, status: :ok, location: @rule_advice }
      else
        format.html { render :edit }
        format.json { render json: @rule_advice.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_rule_advice
      @rule_advice = RuleAdvice.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def rule_advice_params
      params.require(:rule_advice).permit(:value)
    end
end
