class UserAdvicesController < ApplicationController
  before_action :set_user_advice, only: [:edit, :update]
  before_action :authenticate_user!

  # GET /user_advices
  # GET /user_advices.json
  def index
    user = current_user
    @user_advices = user.user_advices
  end

  # GET /user_advices/1/edit
  def edit
  end

  # PATCH/PUT /user_advices/1
  # PATCH/PUT /user_advices/1.json
  def update
    respond_to do |format|
      if @user_advice.update(user_advice_params)
        format.html { redirect_to user_advices_path, notice: 'User advice was successfully updated.' }
        format.json { render :show, status: :ok, location: @user_advice }
      else
        format.html { render :edit }
        format.json { render json: @user_advice.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user_advice
      @user_advice = UserAdvice.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_advice_params
      params.require(:user_advice).permit(:value)
    end
end
