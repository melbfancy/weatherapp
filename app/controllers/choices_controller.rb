class ChoicesController < ApplicationController
  before_action :set_choice, only: [:destroy]
  before_action :authenticate_admin!

  # POST /choices
  # POST /choices.json
  def create
    @advice = Advice.find(params[:advice_id])
    @choice = @advice.choices.create(choice_params)

    respond_to do |format|
      if @choice.save
        format.html { redirect_to advice_path(@advice), notice: 'Choice was successfully created.' }
        format.json { render :show, status: :created, location: @choice }
      else
        format.html { render :new }
        format.json { render json: @choice.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /choices/1
  # DELETE /choices/1.json
  def destroy
    @advice = Advice.find(params[:advice_id])
    @choice = @advice.choices.find(params[:id])
    @choice.destroy

    respond_to do |format|
      format.html { redirect_to advice_path(@advice), notice: 'Choice was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_choice
      @choice = Choice.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def choice_params
      params.require(:choice).permit(:content, :message)
    end
end
