class UsersController < ApplicationController
	before_action :authenticate_admin!

	def index
		@users = User.all
	end
	def show
		@user = User.find(params[:id])
	end

	def update
		@user = User.find(params[:id])
		if @user.update_attributes(user_params)
			redirect_to users_path, notice: 'User was successfully updated.'
		else
			render 'edit'
		end 
	end

	def edit 
		@user = User.find(params[:id])
	end

	def user_params
		params.require(:user).permit(:firstname, :lastname, :email, :phonenumber)
	end
end
