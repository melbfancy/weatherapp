class ApplicationController < ActionController::Base
  
  protect_from_forgery with: :exception
  before_action :configure_devise_permitted_parameters, if: :devise_controller?
  #rescue_from ActiveRecord::RecordNotFound, with: :not_found
  #rescue_from ActionController::RoutingError, with: :not_found
  rescue_from Exception, with: :not_found

  def raise_not_found
    raise ActionController::RoutingError.new("No route matches #{params[:unmatched_route]}")
  end

  def not_found
    render file: "#{Rails.root}/public/404.html", layout: false, status: 404
  end

  protected

  def configure_devise_permitted_parameters
    registration_params = [:firstname, :lastname, :email, :password, :password_confirmation, :phone_number, :station_id, :is_subscribed]

    if params[:action] == 'update'
      devise_parameter_sanitizer.for(:account_update) { 
        |u| u.permit(registration_params << :current_password)
      }
    elsif params[:action] == 'create'
      devise_parameter_sanitizer.for(:sign_up) { 
        |u| u.permit(registration_params) 
      }
    end
  end

end
