class Event_creator
	@weather_data

# Retrieve the weather data
	def self.latest_weather
  		@weather_data = Bom.get_weather2
    end

# Check each rule against the Station weather data
    def self.check_rule_region
    	station_id = nil       
		Rule.all.each do |rule|

# This if statement has been commented out because it is not
# currently being used. It's purpose is too check all stations
# if a rules station is blank.           
            # if rule[:station_id] == nil
            #     Station.all.each do |station|
            #         if station[:ave_max_jan] != nil
            #             station_id = station[:station_bom_id]
            #             @weather_data.each do |data|
            #                 check_specific_rule(rule,data) if station_id == data[:stationID]
            #             end
            #         end
            #     end
            # else

# Check the station identifier of a rule matches a station, if yes
# get the station ID used by Bom and check it against each bit
# of weather data. Where a match is found, compare the data to the rule.            
    			Station.all.each do |station|
                	station_id = station[:station_bom_id]  if rule[:station_id] == station[:id]
                end
                @weather_data.each do |data|
                	check_specific_rule(rule,data) if station_id == data[:stationID]
                end
            # end
		end
    end

# Determine what function to call based off the rule type
# Note: for now only heatwave rule has been implemented
# The default rule that will be executed if hot/cold and type are set
# is multi day heatwave
    def self.check_specific_rule(rule,data)
    	if rule[:hot_or_cold] == true || rule[:hot_or_cold] == nil
            if rule[:rule_type] == "Multi Day" || rule[:rule_type] == nil
                check_multiday_heat(rule,data)
            elsif rule[:rule_type] == "One Day"
                check_singleday_heat(rule,data)
            end                    
        else
            if rule[:rule_type] == "Multi Day" || rule[:rule_type] == nil
                check_multiday_cold(rule,data)
            elsif rule[:rule_type] == "One Day"
                check_singleday_cold(rule,data)
            end
        end
    end

# Check for heatwave event. 
    def self.check_multiday_heat(rule, data)
# Retrieve this months average maximum temperature        
        averageTemp = get_average_monthly_temp(data, true)
        if averageTemp == nil
            return
        end
# Find which days exceed the threshold temperature.        
    	temp_check = data[:maxTemps].map{ |e| e >= (averageTemp + rule[:higher_than_avg])  }
    	length = 0
    	start = -1
        rule_made = false # No events found yet
# loop through the data, true or false now, and try to find consecutive days equal
# to length. Last check after loop is in case an event exists on the last day
    	temp_check.each.with_index do |x,i|
    		if x == true 
    			start = i if start == -1
    			length = length + 1
    		end

    		if x == false 
    			if length >= rule[:length]
    				create_weather_event(rule[:id],start,rule[:length])
                    rule_made = true
    			end
    			start = -1
    			length = 0
    		end
    	end 
    	if length >= rule[:length]
			create_weather_event(rule[:id],start,rule[:length])
            rule_made = true
		end
# If no event was found, remove all events associated with this rule.
# Predictive algorithm so an event may be found early in the week,
# and then not been a threat later on
        if rule_made == false
            WeatherEvent.all.each do |event|
                event.destroy if event[:rule_id] == rule[:id]
            end 
        end
	end

    def self.check_multiday_cold(rule,data)
# Retrieve this months average minimum temperature          
        averageTemp = get_average_monthly_temp(data, false)
        if averageTemp == nil
            return
        end
        #data[:minTemps].shift
# Find which days exceed the threshold temperature.         
        temp_check = data[:minTemps].map{ |e| e <= (averageTemp - rule[:lower_than_avg])  }
        #temp_check.unshift(false)
        length = 0
        start = -1
        rule_made = false # No events found yet
# loop through the data, true or false now, and try to find consecutive days equal
# to length. Last check after loop is in case an event exists on the last day
        temp_check.each.with_index do |x,i|
            if x == true 
                start = i if start == -1
                length = length + 1
            end

            if x == false 
                if length >= rule[:length]
                    create_weather_event(rule[:id],start,rule[:length])
                    rule_made = true
                end
                start = -1
                length = 0
            end
        end
        if length >= rule[:length]
            create_weather_event(rule[:id],start,rule[:length])
            rule_made = true
        end
# If no event was found, remove all events associated with this rule.
# Predictive algorithm so an event may be found early in the week,
# and then not been a threat later on
        if rule_made == false
            WeatherEvent.all.each do |event|
                event.destroy if event[:rule_id] == rule[:id]
            end 
        end
    end

    def self.check_singleday_heat(rule,data)
# Retrieve this months average maximum temperature  
        averageTemp = get_average_monthly_temp(data, true)
        if averageTemp == nil
            return
        end
        prevDay = data[:maxTemps][0..-2]
        currentDay = data[:maxTemps][1..-1]
        data[:maxTemps].shift
        
        temp_check = data[:maxTemps].map{ |e| e >= (averageTemp + rule[:higher_than_avg])}


        shiftedData = currentDay.map.with_index{|x,i| temp_check[i] && ((x - prevDay[i]) >= rule[:tem_dif])}
        rule_made = false
        shiftedData.each.with_index do |x,i|
            if x == true
                create_weather_event(rule[:id],i+1,1) 
                rule_made = true
            end
        end
# If no event was found, remove all events associated with this rule.
# Predictive algorithm so an event may be found early in the week,
# and then not been a threat later on
        if rule_made == false
            WeatherEvent.all.each do |event|
                event.destroy if event[:rule_id] == rule[:id]
            end 
        end
    end

    def self.check_singleday_cold(rule,data)
# Retrieve this months average miniimum temperature  
        averageTemp = get_average_monthly_temp(data, false)
        if averageTemp == nil
            return
        end
        prevDay = data[:minTemps][0..-2]
        #prevDay[0] = 0
        currentDay = data[:minTemps][1..-1]
        data[:minTemps].shift
        temp_check = data[:minTemps].map{ |e| e <= (averageTemp - rule[:lower_than_avg])}

        shiftedData = currentDay.map.with_index{|x,i| temp_check[i] && ((prevDay[i] - x) >= rule[:tem_dif])}

        rule_made = false
        shiftedData.each.with_index do |x,i|
            if x == true
                create_weather_event(rule[:id],i+1,1) 
                rule_made = true
            end
        end
# If no event was found, remove all events associated with this rule.
# Predictive algorithm so an event may be found early in the week,
# and then not been a threat later on
        if rule_made == false
            WeatherEvent.all.each do |event|
                event.destroy if event[:rule_id] == rule[:id]
            end 
        end
    end

# Determine the current month and retrieve the stations average temperature
    def self.get_average_monthly_temp(data, maximun)
        averageTemp = Array.new(12)
        Station.all.each do |station|
            if maximun == true
                if data[:stationID] == station[:station_bom_id]
                    averageTemp[0] = station[:ave_max_jan]
                    averageTemp[1] = station[:ave_max_feb]
                    averageTemp[2] = station[:ave_max_mar]
                    averageTemp[3] = station[:ave_max_apr]
                    averageTemp[4] = station[:ave_max_may]
                    averageTemp[5] = station[:ave_max_june]
                    averageTemp[6] = station[:ave_max_july]
                    averageTemp[7] = station[:ave_max_aug]
                    averageTemp[8] = station[:ave_max_sept]
                    averageTemp[9] = station[:ave_max_oct]
                    averageTemp[10] = station[:ave_max_nov]
                    averageTemp[11] = station[:ave_max_dec]
                end  
            else
                if data[:stationID] == station[:station_bom_id]
                    averageTemp[0] = station[:ave_min_jan]
                    averageTemp[1] = station[:ave_min_feb]
                    averageTemp[2] = station[:ave_min_mar]
                    averageTemp[3] = station[:ave_min_apr]
                    averageTemp[4] = station[:ave_min_may]
                    averageTemp[5] = station[:ave_min_june]
                    averageTemp[6] = station[:ave_min_july]
                    averageTemp[7] = station[:ave_min_aug]
                    averageTemp[8] = station[:ave_min_sept]
                    averageTemp[9] = station[:ave_min_oct]
                    averageTemp[10] = station[:ave_min_nov]
                    averageTemp[11] = station[:ave_min_dec]
                end    
            end
            
        end
        averageTemp[(Time.now.strftime "%m").to_i - 1]
    end

# Create an event in the weather_event table
	def self.create_weather_event(rule_id = nil,starting_in = 0,length = 3)
		time_now = Time.now.strftime "%Y-%m-%d %H:%M:%S"
		start_date = (Time.now+starting_in*24*60*60).strftime "%Y-%m-%d" + ' 09:00:00'
        finish_date = (Time.now+starting_in*24*60*60 + length*24*60*60).strftime "%Y-%m-%d" + ' 20:00:00'

        check_duplication = 0

        WeatherEvent.all.each do |event|
            if event[:rule_id] == rule_id 
                check_duplication = 1
                if event[:time_end] == finish_date && event[:time_start] == start_date
                elsif event[:time_start] == start_date
                    WeatherEvent.update(time_end: finish_date,updated_at: time_now)  
                elsif event[:time_end] == finish_date
                    WeatherEvent.update(time_start: start_date,updated_at: time_now)
                elsif event[:time_start] > start_date && event[:time_end] < finish_date
                    WeatherEvent.update(time_start: start_date,time_end: finish_date,updated_at: time_now)
                elsif event[:time_start] < start_date && event[:time_end] > finish_date
                    WeatherEvent.update(time_start: start_date,time_end: finish_date,updated_at: time_now)  
                else
                    check_duplication = 0
                end                    
            end  
        end

        if check_duplication == 0
            WeatherEvent.create(time_start: start_date,
            time_end: finish_date,
            rule_id: rule_id,
            time_created: time_now,
            time_modified: time_now,
            created_at: time_now,
            updated_at: time_now)
        end
        # p "DUPLICATE" if check_duplication == 1
    	
	end

    def self.check_weather_event_removal()
        deleted = Array.new
        WeatherEvent.all.each do |event|
            time_now = Time.now.strftime "%Y-%m-%d %H:%M:%S"
            event.destroy if event[:time_end] < time_now
            WeatherEvent.all.each do |duplicate|
                if (duplicate[:rule_id] == event[:rule_id] && 
                        duplicate[:time_start] == event[:time_start] && 
                        duplicate[:time_end] == event[:time_end] &&
                        duplicate[:id] != event[:id] &&
                        !(deleted.include? [event[:id],duplicate[:id]]))

                    duplicate.destroy
                    deleted << [duplicate[:id],event[:id]]
                end
            end
        end
    end

end