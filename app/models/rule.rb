class Rule < ActiveRecord::Base
  after_create :linking_advices
  has_many :rule_advices, dependent: :destroy
  belongs_to :station
  has_many :weather_events, dependent: :destroy

  validates :rule_type, presence: true
  validates :tem_dif, numericality: true
  validates :length, numericality: { only_integer: true }
  validates :higher_than_avg, :lower_than_avg, numericality: {greater_than_or_equal_to: 0.0}

  private
  def linking_advices
    RuleAdvice.linking_advices(self)
  end

end
