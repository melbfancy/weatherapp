class Initialiser
  require 'net/ftp'
  require 'open-uri'
  require 'net/http'


# File for retrieving the all the stations names
  CONTENT_LOCATION_STATION = 'ftp.bom.gov.au'
  CONTENT_CHILD_STATION = '/anon/gen/fwo/'
  CONTENT_FILE_STATION = 'IDY02126.dat'

# The template that when combined with the station id provides
# a text file containing the historical data for a station
  CONTENT_LOCATION_AVERAGES = 'http://www.bom.gov.au'
  CONTENT_CHILD_AVERAGES = '/clim_data/cdio/tables/text/'
  CONTENT_FILE_AVERAGES = 'IDCJCM0035_'

# The text that indicates the line to find the values we want
  CONTENT_HIGHTEST_TEMP = 'Mean maximum temperature'
  CONTENT_LOWEST_TEMP = 'Mean minimum temperature'


# This function is used to retrieve a list of all the stations,
# it then collects the past station data if it exists and records
# this information in the station table
  def self.fetch_stations
    stations = Array.new
# Used to catch an error if the connection to the ftp server cant be made    
    begin
#retrieve the file containing all the stations      
      Net::FTP.open(CONTENT_LOCATION_STATION) do |ftp|
        ftp.login
        files = ftp.chdir(CONTENT_CHILD_STATION)
        files = ftp.list('n*')
        station_file = ftp.gettextfile(CONTENT_FILE_STATION, nil)
        station_file = station_file.split(/\n/)
        station_file.delete_at(0) # Delete the first row. Not useful.
# for each row break the row into elements using ',' as the seperator
# strip the useless characters added and the extra whitespace        
        station_file.each do |row|
        	array_element = row.split(',')
        	array_element.each do |element|
        		element.slice! "\""
        		element.slice! "\""
        		element = element.strip
        	end
 # If the station is victorian add it to the station list
 # Add empty tables to be filled by the second half of the function        
        	if array_element.last == 'VIC'
        	  stations << {stationID: array_element[0], stationName: array_element[1].strip, 
                      maxAveTemp: Array.new(12), minAveTemp: Array.new(12)}
          end
        end
      end

# Retrieve the information about monthly average temperatures
# For each station construct the URL 
      stations.each do |row|
        content_file = CONTENT_FILE_AVERAGES + row[:stationID] + '.csv'
        full_location = CONTENT_LOCATION_AVERAGES + CONTENT_CHILD_AVERAGES + content_file

        uri = URI.parse(full_location)
        average_file = Net::HTTP.start(uri.host, uri.port) { |http| http.get(uri.path) }
# make sure the file is retrieved. anything above 400 represents a failed retrieval
        if average_file.code.to_i < 400
# strip useless characters from each row, then find the rows that contain the data
# we need and then add them to the arrays
          row[:stationName] = average_file.body.split(/\n/).first.split()[4..-2].join(' ').tr("'","")
          average_file.body.split(/\n/).each do |text|  
            if text.include? CONTENT_HIGHTEST_TEMP
              row[:maxAveTemp] = text.split(/\"/)[2].split(',')[1..12]
            end

            if text.include? CONTENT_LOWEST_TEMP
              row[:minAveTemp] = text.split(/\"/)[2].split(',')[1..12]
            end
          end
# Add the station to the database. Important to note that if the averages
# werent retrieved the station wont be added.
          time_now = Time.now.strftime "%Y-%m-%d %H:%M:%S"
          Station.create!(
            station_bom_id: row[:stationID],
            name: row[:stationName],
            region: nil,
            ave_max_jan: row[:maxAveTemp][0],
            ave_min_jan: row[:minAveTemp][0],
            ave_max_feb: row[:maxAveTemp][1],
            ave_min_feb: row[:minAveTemp][1],
            ave_max_mar: row[:maxAveTemp][2],
            ave_min_mar: row[:minAveTemp][2],
            ave_max_apr: row[:maxAveTemp][3],
            ave_min_apr: row[:minAveTemp][3],
            ave_max_may: row[:maxAveTemp][4],
            ave_min_may: row[:minAveTemp][4],
            ave_max_june: row[:maxAveTemp][5],
            ave_min_june: row[:minAveTemp][5],
            ave_max_july: row[:maxAveTemp][6],
            ave_min_july: row[:minAveTemp][6],
            ave_max_aug: row[:maxAveTemp][7],
            ave_min_aug: row[:minAveTemp][7],
            ave_max_sept: row[:maxAveTemp][8],
            ave_min_sept: row[:minAveTemp][8],
            ave_max_oct: row[:maxAveTemp][9],
            ave_min_oct: row[:minAveTemp][9],
            ave_max_nov: row[:maxAveTemp][10],
            ave_min_nov: row[:minAveTemp][10],
            ave_max_dec: row[:maxAveTemp][11],
            ave_min_dec: row[:minAveTemp][11],
            created_at: time_now,
            updated_at: time_now
            )

        end
      end
    rescue 
      p "Conenction Error: Data not available"
    end
  end

# Seed two random rules
  def self.random_rules
    time_now = Time.now.strftime "%Y-%m-%d %H:%M:%S"
    Rule.create!( 
      description: "Tester 1",
      higher_than_avg: 0,
      lower_than_avg: 0,
      tem_dif: 10,
      hot_or_cold: true,
      length: 3,
      station_id: 80,
      rule_type: "Multi Day",
      created_at: time_now,
      updated_at: time_now
      )

    Rule.create( 
      description: "Tester 2",
      higher_than_avg: 10,
      lower_than_avg: 0,
      tem_dif: 10,
      hot_or_cold: true,
      length: 3,
      station_id: 80,
      rule_type: "Mulit Day",
      created_at: time_now,
      updated_at: time_now
      )

  end

end
    
