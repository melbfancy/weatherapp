curl -X POST \
-H "Content-Type: application/x-www-form-urlencoded" \
-d "client_id=$1&client_secret=$2&grant_type=client_credentials&scope=SMS" \
"https://api.telstra.com/v1/oauth/token"