curl -H "Content-Type: application/json" \
-H "Authorization: Bearer $1" \
-d "{\"to\":\"$2\", \"body\":\"$3\"}" \
"https://api.telstra.com/v1/sms/messages"