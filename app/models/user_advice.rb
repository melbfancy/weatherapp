require 'user.rb'
require 'advice.rb'
require 'choice.rb'
require 'rule_advice.rb'



# The class contains the methods related to prepare messages to be sent
# Install this gem: https://github.com/javan/whenever
class UserAdvice < ActiveRecord::Base
  belongs_to :user
  belongs_to :advice

  def self.linking_users (advice)
  	User.all.each do |user|
  	  user_advice = advice.user_advices.build(user: user)
  	  user_advice.save
  	end
  end

  def self.linking_advices (user)
  	Advice.all.each do |advice|
  	  user_advice = user.user_advices.build(advice: advice)
  	  user_advice.save
  	end
  end

end
