
CONSUMER_KEY="wiFZA22NxrHOjjb8AVkoGJWB8mm4BSxp"
CONSUMER_SECRET="VDRxo79EiLUyfxL9"

# This class handles all the message sending functionalities
class Mercurius
	# Authenticate consumer_key and consumer_secret to obtain access_token
	def self.authentication
		`sh app/models/authentication.sh #{CONSUMER_KEY} #{CONSUMER_SECRET}`[/"access_token": "(.*?)"/]
		@access_token = $1
	end

	# Use access token to gain authority to send advice message to recipient 
	def self.send message
		unless message.sent
			content = message.content.gsub(/\n/, '"\n"').gsub(/\s/, '" "')
			`sh app/models/sendMessage.sh #{@access_token} #{message.user.phone_number} #{content}`[/(.*)/]
			message.time_sent = DateTime.now
			message.sent = true
			message.save
			# message.content = $1
		end
	end

	# Select all unsent messages in database and send them
	def self.send_all
		Message.all.each { |m| send(m) }
	end

end