class Bom
  require 'nokogiri'
  require 'open-uri'
  require 'json'
  require 'net/ftp'
  require 'open-uri'


  CONTENT_LOCATION_BOM = 'ftp.bom.gov.au'
  CONTENT_CHILD_BOM = '/anon/gen/fwo/'
  CONTENT_FILE_BOM = 'IDY02129.dat'

# This is the old weather data retrieval function, made obselete by the 
# use of stations
  def self.get_weather
    url = 'ftp://ftp2.bom.gov.au/anon/gen/fwo/IDA00003.html'
    doc = Nokogiri::HTML(open(url))
    rows = doc.css('/html/body/table/tr')

    @bom_details = rows.collect do |row|
      bom_detail = {}
      [
        [:site, 'td[1]/text()'],
        [:time_get, 'td[3]/text()'],
        [:temp_h1, 'td[6]/b/text()'],
        [:temp_l1, 'td[5]/b/text()'],
        [:temp_h2, 'td[9]/b/text()'],
        [:temp_l2, 'td[8]/b/text()'],
        [:temp_h3, 'td[12]/b/text()'],
        [:temp_l3, 'td[11]/b/text()'],
        [:temp_h4, 'td[15]/b/text()'],
        [:temp_l4, 'td[14]/b/text()'],
        [:temp_h5, 'td[18]/b/text()'],
        [:temp_l5, 'td[17]/b/text()'],
        [:temp_h6, 'td[21]/b/text()'],
        [:temp_l6, 'td[20]/b/text()'],
        [:temp_h7, 'td[24]/b/text()'],
        [:temp_l7, 'td[23]/b/text()'],
      ].each do |name, xpath|
        bom_detail[name] = row.at_xpath(xpath).to_s.strip.delete(' &nbsp;')
      end
      bom_detail 
    end
    @bom_details 
  end

  
  def self.get_weather2
    weatherData = Array.new
    Station.all.each do |station|
      id = 
      weatherData << {stationID: station[:station_bom_id], 
                      minTemps: Array.new(7), 
                      maxTemps: Array.new(7)}
    end
    begin
      ftp = Net::FTP.new
      ftp.open_timeout = 10
      ftp.connect(CONTENT_LOCATION_BOM)
      ftp.login
      files = ftp.chdir(CONTENT_CHILD_BOM)
      files = ftp.list('n*')
      weather_file = ftp.gettextfile(CONTENT_FILE_BOM, nil)
      file_data = weather_file.split(/\n/)

      # information in the first four rows is not used
      file_data.delete_at(0)
      file_data.delete_at(0)
      file_data.delete_at(0)
      file_data.delete_at(0)

      file_data.each do |row|
        row = row.split(',')
        weatherData.each do |station|
          if row[0].include? station[:stationID] #check if the first element belongs to the station
            if row[1].to_i < 7 # only get the next 7 days
              if row[4].to_f > -100 # file represents nil as -9999.99 only use data if it is valid
                station[:minTemps][row[1].to_i] = row[4].to_f
              end
              if row[3].to_f > -100 # file represents nil as -9999.99 only use data if it is valid
                station[:maxTemps][row[1].to_i] = row[3].to_f
              end
            end
            
          end
        end
      end
      ftp.close  
    rescue 
      p "Conenction Error: Data not available"
    end
    weatherData # return weather data
  end  
end
