class Station < ActiveRecord::Base
	has_many :users
	has_many :rules

	validates :name, presence: true
	validates :station_bom_id, presence: true

	validates :ave_max_jan, :ave_max_feb, :ave_max_mar, :ave_max_apr, :ave_max_may, :ave_max_june, numericality: true
	validates :ave_max_july, :ave_max_aug, :ave_max_sept, :ave_max_oct, :ave_max_nov, :ave_max_dec, numericality: true
	validates :ave_min_jan, :ave_min_feb, :ave_min_mar, :ave_min_apr, :ave_min_may, :ave_min_june, numericality: true
    validates :ave_min_july, :ave_min_aug, :ave_min_sept, :ave_min_oct, :ave_min_nov, :ave_min_dec, numericality: true
end
