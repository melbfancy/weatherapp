class User < ActiveRecord::Base
  after_create :linking_advices
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :user_advices, dependent: :destroy
  has_many :feedbacks
  has_many :messages
  belongs_to :station

  validates :phone_number, presence: true
  validates :station_id, presence: true

  private
  def linking_advices
  	UserAdvice.linking_advices(self)
  end
end
