class Advice < ActiveRecord::Base
	after_create :linking_users, :linking_rules
	has_many :choices, dependent: :destroy
	has_many :rule_advices, dependent: :destroy
	has_many :user_advices, dependent: :destroy
  validates :question, presence: true

	private
  def linking_users
  	UserAdvice.linking_users(self)
  end

  def linking_rules
  	RuleAdvice.linking_rules(self)
  end

end
