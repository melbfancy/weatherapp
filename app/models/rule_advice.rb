class RuleAdvice < ActiveRecord::Base
  belongs_to :rule
  belongs_to :advice

  def self.linking_rules (advice)
  	Rule.all.each do |rule|
  	  rule_advice = advice.rule_advices.build(rule: rule)
  	  rule_advice.save
  	end
  end

  def self.linking_advices (rule)
  	Advice.all.each do |advice|
  	  rule_advice = rule.rule_advices.build(advice: advice)
  	  rule_advice.save
  	end
  end

end
