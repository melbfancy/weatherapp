class Choice < ActiveRecord::Base
  belongs_to :advice
  validates :content, :message, presence: true
end
