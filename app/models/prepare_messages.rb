require 'rufus-scheduler'
UNSUBSCRIBE_URL = 'HeatWave website'

# This class prepares messages and provides functions to send these messages
class PrepareMessages < ActiveRecord::Base
  # Save messages to be sent to all customers who matches
  def self.regular_send_message rule
    user_match_rule(rule)
  end

  # Match messages to users and store them in database
  def self.user_match_rule(rule, event)
    User.all.each do |user|
      message = Message.new
      message.content =""
      message.content += "There will be an event starting from #{event.time_start.to_date} to #{event.time_end.to_date}\n"
      message.user = user
      match_number = 0
      total_number = 0
      user.user_advices.each do |user_advice|
        total_number += 1
        rule.rule_advices.each do |rule_advice|   
          if rule_advice.advice.id == user_advice.advice.id && (user_advice.value.eql?(rule_advice.value) || rule_advice.value == nil || user_advice.value == nil)
            match_number += 1
            user_advice.advice.choices.each do |choice|
              message.content += choice.message if (user_advice.value == choice.content && user_advice.value != nil)
            end
          end
        end
      end
      message.sent = false
      message.content += "Unsubscribe please go to #{UNSUBSCRIBE_URL}"
      message.save if ( (match_number == total_number) && (!message.content.empty?) && user.is_subscribed)
      # message.save if ( (match_number == total_number) && (!message.content.empty?))
    end
  end

  # Search through the event table and check for matching users
  # Event will only be considered if it is happening today or about to happen tomorrow
  def self.user_match_event
    WeatherEvent.all.each do |event|
      today = DateTime.now.to_date
      if today >= (event.time_start.to_date - 1) && today <= event.time_end.to_date
        user_match_rule(event.rule, event)
      end
    end
  end

  # Add a specific message content to user upon user's request
  def self.add_message user, content
    user.user_advices do |user_advice|
      user_advice.advice.choice.value = Choice.find_by(content: content).value if Choice.find_by(content: content)
    end
  end
  
  # Send specific message immediately to a group of user
  def self.urgent_message users, content
    Mercurius.authentication
    users.each do |user|
      message = Message.new
      message.user = user
      message.content = content
      Mercurius.send(message)
    end
  end

  # # Set the day interval to send message (send every x day) and time to send message (in the format of '9:00 am')
  # def self.set_sending_time day_interval, time
  #   @day_interval = day_interval
  #   @time = time
  # end
end
