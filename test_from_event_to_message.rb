require './app/models/prepare_messages.rb'

# 1. Prepare of message for customers as a response to it according to rule
PrepareMessages.user_match_rule(Rule.find(1))
PrepareMessages.user_match_rule(Rule.find(2))
PrepareMessages.user_match_rule(Rule.find(3))

# 2. Send messages to matching users as scheduled
PrepareMessages.scheduled_message

# 3. Call this method to show that user won't receive duplicate messages
PrepareMessages.scheduled_message

# 4. Send urgent messages immediately to selected customer group
PrepareMessages.urgent_message [User.find(1), User.find(2)], "Close you window now!"