# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151015061218) do

  create_table "admins", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "admins", ["email"], name: "index_admins_on_email", unique: true
  add_index "admins", ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true

  create_table "advices", force: :cascade do |t|
    t.string   "description"
    t.string   "question"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "choices", force: :cascade do |t|
    t.string   "content"
    t.string   "message"
    t.integer  "advice_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "choices", ["advice_id"], name: "index_choices_on_advice_id"

  create_table "feedbacks", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.integer  "user_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "feedbacks", ["user_id"], name: "index_feedbacks_on_user_id"

  create_table "messages", force: :cascade do |t|
    t.text     "content"
    t.boolean  "sent"
    t.datetime "time_sent"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "messages", ["user_id"], name: "index_messages_on_user_id"

  create_table "rule_advices", force: :cascade do |t|
    t.string   "value"
    t.integer  "rule_id"
    t.integer  "advice_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "rule_advices", ["advice_id"], name: "index_rule_advices_on_advice_id"
  add_index "rule_advices", ["rule_id"], name: "index_rule_advices_on_rule_id"

  create_table "rules", force: :cascade do |t|
    t.string   "description"
    t.decimal  "high_tem"
    t.decimal  "low_tem"
    t.decimal  "higher_than_avg"
    t.decimal  "lower_than_avg"
    t.boolean  "hot_or_cold"
    t.integer  "length"
    t.decimal  "tem_dif"
    t.string   "rule_type"
    t.integer  "station_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "rules", ["station_id"], name: "index_rules_on_station_id"

  create_table "stations", force: :cascade do |t|
    t.string   "station_bom_id"
    t.string   "name"
    t.string   "region"
    t.decimal  "ave_max_jan"
    t.decimal  "ave_min_jan"
    t.decimal  "ave_max_feb"
    t.decimal  "ave_min_feb"
    t.decimal  "ave_max_mar"
    t.decimal  "ave_min_mar"
    t.decimal  "ave_max_apr"
    t.decimal  "ave_min_apr"
    t.decimal  "ave_max_may"
    t.decimal  "ave_min_may"
    t.decimal  "ave_max_june"
    t.decimal  "ave_min_june"
    t.decimal  "ave_max_july"
    t.decimal  "ave_min_july"
    t.decimal  "ave_max_aug"
    t.decimal  "ave_min_aug"
    t.decimal  "ave_max_sept"
    t.decimal  "ave_min_sept"
    t.decimal  "ave_max_oct"
    t.decimal  "ave_min_oct"
    t.decimal  "ave_max_nov"
    t.decimal  "ave_min_nov"
    t.decimal  "ave_max_dec"
    t.decimal  "ave_min_dec"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "user_advices", force: :cascade do |t|
    t.string   "value"
    t.integer  "user_id"
    t.integer  "advice_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "user_advices", ["advice_id"], name: "index_user_advices_on_advice_id"
  add_index "user_advices", ["user_id"], name: "index_user_advices_on_user_id"

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "firstname"
    t.string   "lastname"
    t.string   "phone_number"
    t.integer  "station_id"
    t.boolean  "is_subscribed"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  add_index "users", ["station_id"], name: "index_users_on_station_id"

  create_table "weather_events", force: :cascade do |t|
    t.datetime "time_start"
    t.datetime "time_end"
    t.datetime "time_created"
    t.datetime "time_modified"
    t.integer  "rule_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "weather_events", ["rule_id"], name: "index_weather_events_on_rule_id"

end
