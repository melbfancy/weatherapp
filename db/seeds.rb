# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# Advices
Advice.create!(description: "Heart Disease", question: "Do you have Heart Disease?")
Advice.create!(description: "Air Conditioner", question: "Do you have Air Conditioner?")
Advice.create!(description: "Window", question: "Do you have Window?")
Advice.create!(description: "House", question: "Do you have a House?")

# Choices
Choice.create!(content: "Yes", message: "Use Heart \n", advice_id: 1)
Choice.create!(content: "No", message: "No Heart \n", advice_id: 1)
Choice.create!(content: "Yes", message: "Use AirCon \n", advice_id: 2)
Choice.create!(content: "No", message: "No AirCon \n", advice_id: 2)
Choice.create!(content: "Yes", message: "Use Window \n", advice_id: 3)
Choice.create!(content: "No", message: "No Window \n", advice_id: 3)
Choice.create!(content: "Yes", message: "Use House \n", advice_id: 4)
Choice.create!(content: "No", message: "No House \n", advice_id: 4)


# Rules
Rule.create!(description: "rule 01", high_tem: 30, low_tem:1, higher_than_avg: 10, lower_than_avg: 10, hot_or_cold: true, length: 1, tem_dif: 3.5, rule_type: "One Day", station_id: 1)
Rule.create!(description: "rule 02", high_tem: 40, low_tem:0, higher_than_avg: 10, lower_than_avg: 10, hot_or_cold: false, length: 2, tem_dif: 4.5, rule_type: "Multi Day", station_id: 1)
Rule.create!(description: "rule 03", high_tem: 30, low_tem:0, higher_than_avg: 10, lower_than_avg: 10, hot_or_cold: true, length: 3, tem_dif: 5.5, rule_type: "One Day", station_id: 1)
Rule.create!(description: "rule 04", high_tem: 40, low_tem:5, higher_than_avg: 10, lower_than_avg: 10, hot_or_cold: false, length: 4, tem_dif: 4.5, rule_type: "Multi Day", station_id: 1)
Rule.create!(description: "rule 05", high_tem: 30, low_tem:0, higher_than_avg: 10, lower_than_avg: 10, hot_or_cold: true, length: 3, tem_dif: 0.5, rule_type: "One Day", station_id: 1)
Rule.create!(description: "rule 06", high_tem: 50, low_tem:3, higher_than_avg: 10, lower_than_avg: 10, hot_or_cold: false, length: 2, tem_dif: 5.5, rule_type: "Multi Day", station_id: 1)
Rule.create!(description: "rule 07", high_tem: 30, low_tem:0, higher_than_avg: 10, lower_than_avg: 10, hot_or_cold: true, length: 1, tem_dif: 3.5, rule_type: "One Day", station_id: 1)
Rule.create!(description: "rule 08", high_tem: 45, low_tem:1, higher_than_avg: 10, lower_than_avg: 10, hot_or_cold: false, length: 5, tem_dif: 0.5, rule_type: "Multi Day", station_id: 1)
Rule.create!(description: "testing rule", higher_than_avg: 1, lower_than_avg: 1, hot_or_cold: false, length: 2, tem_dif: 0.1, rule_type: "Multi Day", station_id: 1)


# Users
User.create!(phone_number: "0478136452", email: 'test1@test.com', password: '12345678', station_id: 1, is_subscribed: true)
User.create!(phone_number: "0478136452", email: 'test2@test.com', password: '12345678', station_id: 1, is_subscribed: true)
User.create!(phone_number: "0478136452", email: 'test3@test.com', password: '12345678', station_id: 2, is_subscribed: false)


# UserAdvices for User 1
ua = UserAdvice.find_by(user_id: 1, advice_id: 1)
ua.value = "Yes"
ua.save
ua = UserAdvice.find_by(user_id: 1, advice_id: 2)
ua.value = "Yes"
ua.save
ua = UserAdvice.find_by(user_id: 1, advice_id: 3)
ua.value = "Yes"
ua.save
ua = UserAdvice.find_by(user_id: 1, advice_id: 4)
ua.value = "No"
ua.save

# UserAdvices for User 2
ua = UserAdvice.find_by(user_id: 2, advice_id: 1)
ua.value = "Yes"
ua.save
ua = UserAdvice.find_by(user_id: 2, advice_id: 2)
ua.value = "Yes"
ua.save
ua = UserAdvice.find_by(user_id: 2, advice_id: 3)
ua.value = "Yes"
ua.save
ua = UserAdvice.find_by(user_id: 2, advice_id: 4)
ua.value = nil
ua.save

# UserAdvices for User 3
ua = UserAdvice.find_by(user_id: 3, advice_id: 1)
ua.value = "Yes"
ua.save
ua = UserAdvice.find_by(user_id: 3, advice_id: 2)
ua.value = "Yes"
ua.save
ua = UserAdvice.find_by(user_id: 3, advice_id: 3)
ua.value = "Yes"
ua.save
ua = UserAdvice.find_by(user_id: 3, advice_id: 4)
ua.value = "Yes"
ua.save

# RuleAdvices for Rule 1
ra = RuleAdvice.find_by(rule_id: 1, advice_id: 1)
ra.value = "Yes"
ra.save
ra = RuleAdvice.find_by(rule_id: 1, advice_id: 2)
ra.value = "Yes"
ra.save
ra = RuleAdvice.find_by(rule_id: 1, advice_id: 3)
ra.value = "Yes"
ra.save
ra = RuleAdvice.find_by(rule_id: 1, advice_id: 4)
ra.value = "No"
ra.save

# RuleAdvices for Rule 2
ra = RuleAdvice.find_by(rule_id: 2, advice_id: 1)
ra.value = "Yes"
ra.save
ra = RuleAdvice.find_by(rule_id: 2, advice_id: 2)
ra.value = "Yes"
ra.save
ra = RuleAdvice.find_by(rule_id: 2, advice_id: 3)
ra.value = "Yes"
ra.save
ra = RuleAdvice.find_by(rule_id: 2, advice_id: 4)
ra.value = "No"
ra.save

# RuleAdvices for Rule 3
ra = RuleAdvice.find_by(rule_id: 3, advice_id: 1)
ra.value = "Yes"
ra.save
ra = RuleAdvice.find_by(rule_id: 3, advice_id: 2)
ra.value = "Yes"
ra.save
ra = RuleAdvice.find_by(rule_id: 3, advice_id: 3)
ra.value = "Yes"
ra.save
ra = RuleAdvice.find_by(rule_id: 3, advice_id: 4)
ra.value = "No"
ra.save




