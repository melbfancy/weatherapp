class CreateRuleAdvices < ActiveRecord::Migration
  def change
    create_table :rule_advices do |t|
      t.string :value
      t.references :rule, index: true, foreign_key: true
      t.references :advice, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
