class CreateRules < ActiveRecord::Migration
  def change
    create_table :rules do |t|
      t.string :description
      t.decimal :high_tem
      t.decimal :low_tem
      t.decimal :higher_than_avg
      t.decimal :lower_than_avg
      t.boolean :hot_or_cold
      t.integer :length
      t.decimal :tem_dif
      t.string :rule_type
      t.references :station, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
