class CreateChoices < ActiveRecord::Migration
  def change
    create_table :choices do |t|
      t.string :content
      t.string :message
      t.references :advice, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
