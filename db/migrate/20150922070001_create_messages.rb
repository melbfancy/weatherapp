class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.text :content
      t.boolean :sent
      t.datetime :time_sent
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
