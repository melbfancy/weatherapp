class CreateStations < ActiveRecord::Migration
  def change
    create_table :stations do |t|
      t.string :station_bom_id
      t.string :name
      t.string :region
      t.decimal :ave_max_jan
      t.decimal :ave_min_jan
      t.decimal :ave_max_feb
      t.decimal :ave_min_feb
      t.decimal :ave_max_mar
      t.decimal :ave_min_mar
      t.decimal :ave_max_apr
      t.decimal :ave_min_apr
      t.decimal :ave_max_may
      t.decimal :ave_min_may
      t.decimal :ave_max_june
      t.decimal :ave_min_june
      t.decimal :ave_max_july
      t.decimal :ave_min_july
      t.decimal :ave_max_aug
      t.decimal :ave_min_aug
      t.decimal :ave_max_sept
      t.decimal :ave_min_sept
      t.decimal :ave_max_oct
      t.decimal :ave_min_oct
      t.decimal :ave_max_nov
      t.decimal :ave_min_nov
      t.decimal :ave_max_dec
      t.decimal :ave_min_dec

      t.timestamps null: false
    end
  end
end
