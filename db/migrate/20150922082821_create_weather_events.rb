class CreateWeatherEvents < ActiveRecord::Migration
  def change
    create_table :weather_events do |t|
      t.datetime :time_start
      t.datetime :time_end
      t.datetime :time_created
      t.datetime :time_modified
      t.references :rule, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
