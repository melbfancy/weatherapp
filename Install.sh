# Update yum installer
sudo yum update

# Install necessary libraries needed for Ruby on Rails
sudo yum install gcc-c++ patch readline readline-devel zlib zlib-devel
sudo yum install libyaml-devel libffi-devel openssl-devel make
sudo yum install bzip2 autoconf automake libtool bison iconv-devel
sudo yum install httpd-devel apr-devel sqlite-devel
sudo yum install bundler

# Install Ruby Version Manager
curl -L get.rvm.io | bash -s stable
source /etc/profile.d/rvm.sh

# Install Ruby version 2.2.2 and set as default ruby version
rvm install 2.2.2
rvm use 2.2.2 -- default

# Make sure rubygems is installed
sudo yum install rubygems
# Update gem
sudo gem update
# Install rails gem
sudo gem install rails -V
# Install necessary gems with bundler
bundle install

echo "Installation Complete"
