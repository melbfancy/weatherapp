# Ensure tables are setup up
rake db:migrate
# Remove all table element and seed tables
rake db:reset
# Retrieve stations from the BOM website and seed them into
# the database
rails r "Initialiser.fetch_stations"