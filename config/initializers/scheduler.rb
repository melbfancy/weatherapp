require 'rufus-scheduler'
scheduler = Rufus::Scheduler.new

# scheduler.in '1s' do
# 	Event_creator.latest_weather
# 	Event_creator.check_rule_region
# 	PrepareMessages.user_match_event
# 	Mercurius.authentication
#  Mercurius.send_all
# end

# Send messages every day at 09h00
scheduler.cron '00 09 * * *' do
  Mercurius.authentication
  Mercurius.send_all
end

scheduler.cron '00 06 * * *' do
	Event_creator.latest_weather
	Event_creator.check_rule_region
	PrepareMessages.user_match_event
end
