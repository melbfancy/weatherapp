Rails.application.routes.draw do
  resources :feedbacks
  resources :weather_events
  resources :stations
  resources :messages
  resources :user_advices
  devise_for :admins
  devise_for :users
  resources :users
  resources :admins

  root 'static_pages#home'

  resources :advices do
    resources :choices
  end
  resources :rules do
    resources :rule_advices
  end

  get '*unmatched_route', to: 'application#raise_not_found'

end
